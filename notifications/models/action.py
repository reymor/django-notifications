# coding=utf-8
from django.db import models

ACTION_CACHE = {}


class ActionManager(models.Manager):
    
    def get_by_natural_key(self, name):
        if name in ACTION_CACHE and not isinstance(ACTION_CACHE.get(name), self.model):
            attributes = ACTION_CACHE[name]
            try:
                ACTION_CACHE[name] = self.get(name=name)
            except self.model.DoesNotExist:
                ACTION_CACHE[name] = self.model(name=name)
                ACTION_CACHE[name].read_as = attributes['read_as']
                ACTION_CACHE[name].description = attributes['description']
                ACTION_CACHE[name].save()
        return ACTION_CACHE.get(name, None)


class Action(models.Model):
    name = models.CharField(max_length=50, unique=True)
    read_as = models.CharField(max_length=50)
    description = models.CharField(max_length=200, blank=True, default='')

    objects = ActionManager()

    class Meta:
        app_label = 'notifications'

    def __unicode__(self):
        return self.read_as

    def natural_key(self):
        return self.name,
