# coding=utf-8
import logging
from django.db import ProgrammingError
from notifications import create_event
from notifications.models import (
    EventType, EVENT_TYPE_CACHE,
    Action, ACTION_CACHE,
    EventTypeCategory, CATEGORY_CACHE,
    Transport,
    EventAttendantsConfig,
    NotificationTemplateConfig
)

logger = logging.getLogger(__name__)


"""
EXAMPLE_NOTIFICATIONS_VALUE = {
    'notifications.transports.FeedTransport': {
        'attendants': [
            {
                'source': 'target_obj',
                'type': 'property',
                'value': 'owner,owner'
            },
            {
                'source': 'event',
                'value': 'user,acting_user'
            },
            {
                'source': '',
                'value': 'my_app.utils.user_followers'
            }
        ],
        'templates': {
            'primary_context': {
                'single_template_path': None,
                'data': None,
                'template_path': 'notifications/feed/primary_context/action.html'
            },
            'secondary_context': {
                'single_template_path': None,
                'data': None,
                'template_path': 'notifications/feed/secondary_context/action.html'
            }
        }

    },
    'notifications.transports.EmailTransport': {
        'attendants': [
            {
                'source': 'target_obj',
                'type': 'property',
                'value': 'owner,owner'
            }
        ],
        'templates': {
            'email': {
                'single_template_path': 'notifications/email/single/action.html',
                'data': {
                    'subject': '%(user)s ha interactuado con %(target)s'
                },
                'template_path': 'notifications/email/action.html'
            }
        }
    }
}
"""


def notifier(
        event_name,
        read_as=None,
        action_name='default',
        target_type='all',
        event_category='default',
        notifications=None,
        event_immediate=False,
        action_details=None,
        category_details=None
):
    """
    The decorated function must return either a tuple or a dict, containing:
        user: the acting user,
        target: the target object,
        details: text details for the event to be created,
        extra_data [optional],
        related_objects [optional]

    ** In case of a tuple, of course, the order is critical!

    """

    def decorator(func):
        if action_name in ACTION_CACHE and action_details:
            ACTION_CACHE[action_name]['read_as'] = action_details.get('read_as', ACTION_CACHE[action_name]['read_as'])
            ACTION_CACHE[action_name]['description'] = action_details.get('description', ACTION_CACHE[action_name]['description'])
        elif action_name not in ACTION_CACHE:
            ACTION_CACHE[action_name] = {
                'read_as': action_details.get('read_as', action_name) if action_details else action_name,
                'description': action_details.get('description', '') if action_details else ''
            }

        if event_category in CATEGORY_CACHE and category_details:
            CATEGORY_CACHE[event_category]['read_as'] = category_details.get('read_as', CATEGORY_CACHE[event_category]['read_as'])
        elif event_category not in CATEGORY_CACHE:
            CATEGORY_CACHE[event_category] = {
                'read_as': category_details.get('read_as', event_category) if category_details else event_category,
            }

        if event_name in EVENT_TYPE_CACHE:
            if read_as:
                EVENT_TYPE_CACHE[event_name]['read_as'] = read_as
            if action_name:
                EVENT_TYPE_CACHE[event_name]['action'] = action_name
            if event_category:
                EVENT_TYPE_CACHE[event_name]['category'] = event_category
            if event_immediate:
                EVENT_TYPE_CACHE[event_name]['immediate'] = event_immediate
        else:
            EVENT_TYPE_CACHE[event_name] = {
                'read_as': read_as or event_name,
                'action': action_name,
                'target_type': target_type,
                'category': event_category,
                'immediate': event_immediate
            }

        notifications_dict = notifications or {}

        def func_wrapper(*args, **kwargs):
            event_type = EventType.objects.get_by_natural_key(event_name)
            if event_type:
                for transport, settings in notifications_dict.items():
                    try:
                        transport = Transport.objects.get_by_natural_key(transport)
                    except Transport.DoesNotExist:
                        continue

                    attendants = settings.setdefault('attendants', [])
                    if not EventAttendantsConfig.objects.filter(
                        event_type=event_type,
                        transport=transport,
                    ).update(get_attendants_methods=attendants):
                        EventAttendantsConfig.objects.create(
                            event_type=event_type,
                            transport=transport,
                            get_attendants_methods=attendants
                        )

                    templates = settings.setdefault('templates', {})
                    for context, templates_settings in templates.items():
                        if not NotificationTemplateConfig.objects.filter(
                            event_type=event_type,
                            transport=transport,
                            context=context
                        ).update(**templates_settings):
                            NotificationTemplateConfig.objects.create(
                                event_type=event_type,
                                transport=transport,
                                context=context,
                                **templates_settings
                            )

            result = func(*args, **kwargs)
            if isinstance(result, tuple):
                result = list(result)
                result.insert(1, event_type)
                create_event(*result)
            elif isinstance(result, dict):
                result.update({'event_type': event_type})
                create_event(**result)
            else:
                logger.error(
                    'Skipping event creation because incorrect arguments were provided by the decorated function.'
                )
        return func_wrapper
    return decorator
